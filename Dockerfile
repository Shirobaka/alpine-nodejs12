FROM        node:12-alpine

LABEL       author="Manuel Goette" maintainer="info@six-gaming.com"

RUN         apk add --no-cache --update libc6-compat ffmpeg git \
            && adduser -D -h /home/container container

USER        container
ENV         USER=container HOME=/home/container
WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh
CMD         ["/bin/ash", "/entrypoint.sh"]
